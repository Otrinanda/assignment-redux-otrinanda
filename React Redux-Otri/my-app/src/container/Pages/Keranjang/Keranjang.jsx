import React, { Component } from 'react'
import { connect } from 'react-redux'
import './Keranjang.css'

class Keranjang extends Component {
    render() {
        return (
            <div className="parentbox">
                <h3 className="title">Keranjang Penilaian</h3>
               
                {/* dispatchDipanggildisini */}
                <p className="body">Total Penilaian : {this.props.order}</p>
            </div >
        )
    }
}

const mapStateToProps = state => {
    return {
        order: state.totalOrder
    }
}

export default connect(mapStateToProps)(Keranjang)