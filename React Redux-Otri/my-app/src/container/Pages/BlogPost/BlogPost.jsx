import React, { Component, Fragment } from 'react'
import './BlogPost.css'
import Post from '../../../component/Post/Post'
import axios from 'axios'

class BlogPost extends Component {
    state = {
        post: [],
        formBlogPost: {
            id: 1,
            nama: '',
            posisi: '',
            kontak: '',
            alamat: '',
            userId: 1
        },
        isUpdate: false
    }

    getPostAPI = () => {
        axios.get(' http://localhost:3004/posts?_sort=id&_order=desc')
            .then((result) => {
                console.log(result.data)
                this.setState({
                    post: result.data
                })
            })
    }

    postDataToAPI = () => {
        axios.post(' http://localhost:3004/posts', this.state.formBlogPost)
            .then((result) => {
                console.log(result);
                this.getPostAPI();
                this.setState({
                    isUpdate: false,
                    formBlogPost: {
                        id: 1,
                        nama: '',
                        posisi: '',
                        kontak: '',
                        alamat: '',
                        userId: 1
                    }
                })
            }, (err) => {
                console.log('Error: ' + err)
            })
    }

    putDataToAPI = () => {
        axios.put(`http://localhost:3004/posts/${this.state.formBlogPost.id}`, this.state.formBlogPost)
            .then((result) => {
                console.log(result);
                this.getPostAPI();
                this.setState({
                    isUpdate: false,
                    formBlogPost: {
                        id: 1,
                        nama: '',
                        posisi: '',
                        kontak: '',
                        alamat: '',
                        userId: 1
                    }
                })
            })
    }

    handleRemove = (data) => {
        axios.delete(`http://localhost:3004/posts/${data}`)
            .then((res) => {
                this.getPostAPI()
            })
    }

    handleUpdate = (data) => {
        console.log(data)
        this.setState({
            formBlogPost: data,
            isUpdate: true
        })

    }

    handleFormChange = (event) => {
        let formBlogPostNew = { ...this.state.formBlogPost }
        formBlogPostNew[event.target.name] = event.target.value
        let timestamp = new Date().getTime();
        if (!this.state.isUpdate) {
            formBlogPostNew['id'] = timestamp;
        }
        //let title = event.target.value
        this.setState({
            formBlogPost: formBlogPostNew
        },
            // ()=>{
            //     console.log('value',this.state.formBlogPost)} 
        )
    }

    handleSubmit = () => {
        if (this.state.isUpdate) {
            this.putDataToAPI()
        }
        else {
            this.postDataToAPI();
        }
    }

    handleDetail = (id) => {
        this.props.history.push(`detail-post/${id}`)
    }

    componentDidMount() {
        this.getPostAPI();
    }

    render() {
        return (
            <Fragment>
                <div className="Parentbox">
                    <div className="Form">
                        {this.state.post.map(post => {
                            return (
                                <Post
                                    key={post.id} data={post}
                                    remove={this.handleRemove} update={this.handleUpdate}
                                    goDetail={this.handleDetail}
                                />
                            )
                        })
                        }
                        <p className="ket" >jika data tidak muncul, maka run ini di my-app: json-server --watch db.json --port 3004</p>
                    </div>
                    <div className="Data">

                        <div className="FormAddPost">
                            <label htmlFor="nama" >Nama</label>
                            <input type="text" name='nama' value={this.state.formBlogPost.nama}
                                placeholder='add name here' onChange={this.handleFormChange} />
                            <label htmlFor="posisi" >Posisi</label>
                            <input type="text" name='posisi' value={this.state.formBlogPost.posisi}
                                placeholder='add postion here' onChange={this.handleFormChange} />
                            <label htmlFor="kontak" >Kontak </label>
                            <input type="text" name='kontak' value={this.state.formBlogPost.kontak}
                                placeholder='add contact here' onChange={this.handleFormChange} />

                            <label htmlFor="alamat" >Alamat</label>
                            <textarea
                                name="alamat" id="alamat" cols="30" rows="10" value={this.state.formBlogPost.alamat}
                                placeholder='add address here ' onChange={this.handleFormChange} />

                            <button className="btn-submit" onClick={this.handleSubmit}>SAVE</button>
                        </div>
                    </div>
                </div>
            </Fragment>

        )
    }
}

export default BlogPost