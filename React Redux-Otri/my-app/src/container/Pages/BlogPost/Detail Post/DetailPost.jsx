import React, { Component, Fragment } from 'react';
import Axios from 'axios';
import './DetailPost.css'
import { connect } from 'react-redux'

class DetailPost extends Component {

    state = {
        post: {
            title: '',
            body: ' '
        }
    }

    componentDidMount() {

        let id = this.props.match.params.postID
        Axios.get(`http://localhost:3004/posts/${id}`)
            .then(result => {
                console.log('result ', result)

                let post = result.data;

                this.setState({
                    post: {
                        nama: post.nama,
                        posisi: post.posisi,
                        kontak: post.kontak,
                        alamat: post.alamat
                    }
                })
            })
    }

    render() {

        return (
            <Fragment>
                <div className="parentbox">
                    <div className="img-thumb" >
                        <img src="https://placeimg.com/280/360/people" alt="photo here,please connect internet" />
                    </div>
                    <div className="p-detail-post">
                        <p className="detail-title">{this.state.post.nama}</p>
                        <p className="detail-body">{this.state.post.posisi}</p>
                        <p className="detail-body">{this.state.post.kontak}</p>
                        <p className="detail-body">{this.state.post.alamat}</p>
                        <div className="Counter">
                            {/* dispatchDpanggildisini */}
                            <p className="textcounter">Berikan Penilaian Anda</p>
                            <button className="minus" onClick={this.props.handleMinus}>-</button>
                            <input type="text" value={this.props.order} />
                            <button className="plus" onClick={this.props.handlePlus}>+</button>
                        </div>
                    </div>
                </div>

            </Fragment>
        )
    }
}

//dispatch ambil
const mapsStateToProps = (state) => {
    return {
        order: state.totalOrder
    }
}

//dispatch taruh
const mapsDispatchToProps = (dispatch) => {
    return {
        handlePlus: () => dispatch({ type: 'PLUS_ORDER' }),
        handleMinus: () => dispatch({ type: 'MINUS_ORDER' }),
    }
}


export default connect(mapsStateToProps, mapsDispatchToProps)(DetailPost)