import React, { Component, Fragment } from 'react'
import { BrowserRouter, Route, Link } from 'react-router-dom'
import BlogPost from '../Pages/BlogPost/BlogPost'
import Keranjang from '../Pages/Keranjang/Keranjang'
import DetailPost from '../Pages/BlogPost/Detail Post/DetailPost'

import './Home.css'

class Home extends Component {

    state = {
        showComponent: true
    }

    render() {
        return (

            <BrowserRouter>
                <Fragment>
                    <div className="navigation">
                        <div className="parentbox">
                            <div className="Logo">
                                <p>XSIS Mitra Utama</p>
                            </div>
                            <div className="Link">
                                <Link to="/">Post</Link>
                                <Link to="/Keranjang">Keranjang</Link>
                            </div>
                        </div>
                    </div>
                    <Route path="/" exact component={BlogPost} />
                    <Route path="/detail-post/:postID" component={DetailPost} />
                    <Route path="/Keranjang" component={Keranjang} />
                </Fragment>
            </BrowserRouter>

        )
    }
}

export default Home