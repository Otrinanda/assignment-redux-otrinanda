import React from 'react';

const Post = (props) => {
    return (
        <div className="Post">
            <div className="img-thumb" >
                <img src="https://placeimg.com/200/150/people" alt="photo here,please connect internet" />
            </div>
            <div className="Content">
                <p className="title" onClick={()=>props.goDetail(props.data.id)}>{props.data.nama}</p>
                <p className="desc">{props.data.posisi}</p>
                <button className='Update' onClick={()=>props.update(props.data)} >UPDATE</button>
                <button className='Remove' onClick={()=>props.remove(props.data.id)} >REMOVE</button>
            </div>
        </div>
    )
}



export default Post